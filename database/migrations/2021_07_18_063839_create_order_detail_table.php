<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('tablet_id')->default(null)->nullable();
            $table->integer('phone_id')->default(null)->nullable();
            $table->integer('laptop_id')->default(null)->nullable();
            $table->integer('headphone_id')->default(null)->nullable();
            $table->integer('order_id')->nullable(false);
            $table->integer('price')->nullable();
            $table->integer('quantity')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->text('description', 500)->default('');

            $table->dateTime('join_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_detail');
    }
}
