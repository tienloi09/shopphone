@extends('layouts.admin.app')

@section('title', 'Danh sách sản phẩm của đơn hàng')

@section('main-content')


<form method="post">
    @csrf
    @method('POST')

    <div class="card">
        <div class="card-body p-0">
            <div class="table-responsive">
                <table class="table m-0">
                    <thead>
                        <tr>

                            <th width="2%">#</th>
                            <th>Tên sản phẩm</th>
                            <th>Số lượng</th>
                            <th>Giá tiền</th>
                            <th width="12%">Trạng thái</th>

                            <th width="12%">
                                <form action="">Chức năng</form>
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($order_detail as $key => $item)
                        <tr>
                            <td>{{ $key +=1 }}</td>
                            @if($item->phone_id)
                            @foreach ($phones as $phone)
                            @if($item->phone_id == $phone->id)
                            <td>{{$phone->name}}</td>
                            @endif
                            @endforeach
                            @endif
                            @if($item->laptop_id)
                            @foreach ($laptops as $laptop)
                            @if($item->laptop_id == $laptop->id)
                            <td>{{$laptop->name}}</td>
                            @endif
                            @endforeach
                            @endif


                            <td>{{$item->quantity}}</td>
                            <td>{{ currency_format($item->price)}}</td>
                            <td>{{$item->status}}</td>

                            <td>
                                <form action="{{ route('admin.order_detail.destroy', $item->id) }}" method="POST" style="float: left; margin-right: 7px">
                                    <a class="btn btn-success btn-sm mr-1" href="{{ route('admin.order_detail.show', $item->id) }}"><i class="far fa-list-alt"></i></a>
                                    <a class="btn btn-success btn-sm mr-1" href="{{ route('admin.order_detail.edit', $item->id) }}"><i class="fas fa-pencil-alt"></i></a>
                                    <a href="javascript:void(0)" class="btn btn-danger btn-sm delete-d" id="{{ $item->id }}" data="order_detail"><i class="fas fa-trash"></i></a>
                                </form>

                                {{-- //Modal Delete --}}
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">
                                                    Bạn có muốn xóa không ?
                                                </h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <h5>Bạn có chắc chắn không ?</h5>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
                                                <button type="button" class="btn btn-success" id="confirm-delete">Đồng ý</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </td>
                        </tr>


                        @endforeach

                    </tbody>
                </table>

            </div>
        </div>
    </div>
</form>


@endsection


<?php
function currency_format($number, $suffix = '')
{
    if (!empty($number)) {
        return number_format($number, 0, ',', ',') . "{$suffix}";
    }
}
?>