@extends('layouts.admin.app')

@section('title', 'Đơn hàng')

@section('main-content')

@if ($errors->any())
<div class="alert alert-danger border-left-danger" role="alert">
    <ul class="pl-4 my-2">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="row">
    <div class="col-lg-12 order-lg-1">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-success">Thông tin đơn hàng</h6>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('admin.orders.store') }}" autocomplete="off" id="myform">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group focused">
                                    <label class="form-control-label" for="full_name">Tên khách hàng<span class="small text-danger">*</span></label>
                                    <input type="text" id="name" class="form-control" name="full_name" value="{{ old('full_name') }}" maxlength="500" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group focused">
                                    <label class="form-control-label" for="phone">Số điện thoại<span class="small text-danger">*</span></label>
                                    <input type="text" id="name" class="form-control" name="phone" value="{{ old('phone') }}" maxlength="500" required>
                                </div>
                            </div>


                        </div>
                        <div class="row ">
                            <div class="col-lg-6">
                                <div class="form-group focused">
                                    <label class="form-control-label" for="email">Email<span class="small text-danger">*</span></label>
                                    <input type="email" id="name" class="form-control" name="email" value="{{ old('email') }}" maxlength="500" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label" for="gender">Giới tính</label>
                                <select class="form-control custom-select" name="gender" style="width:100%;">
                                    <option value="0" @if (old('gender')==0) selected="selected" @endif>
                                        Nữ</option>
                                    <option value="1" @if (old('gender')==1) selected="selected" @endif>
                                        Nam</option>

                                </select>
                            </div>
                        </div>
                       
                        <div class="row">

                            <div class="col-lg-12 ">
                                <div class="form-group focused">
                                    <label class="form-control-label" for="address">Địa chỉ
                                        <span class="small text-danger">*</span>
                                    </label>
                                    <textarea rows="5" id="address" class="form-control" name="address" maxlength="500" required>{{ old('address') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <hr class="bg-success">
                        <div class="row">
                            <div class="col-lg-6">
                                <label class="form-control-label" for="status">Trạng thái</label>
                                <select class="form-control custom-select" name="status" style="width:100%;">
                                    <option value="0" @if (old('status')==0) selected="selected" @endif>
                                        Mới</option>
                                    <option value="1" @if (old('status')==1) selected="selected" @endif>
                                        Thành công</option>
                                    <option value="2" @if (old('status')==2) selected="selected" @endif>
                                        Thất bại</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <label class="form-control-label" for="payment_methods">Phương thức thanh toán</label>
                                <select class="form-control custom-select" name="payment_methods" style="width:100%;">
                                    <option value="0" @if (old('payment_methods')==0) selected="selected" @endif>
                                        Chuyển khoản</option>
                                    <option value="1" @if (old('payment_methods')==1) selected="selected" @endif>
                                        Tiền mặt</option>

                                </select>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group focused">
                                    <label class="form-control-label" for="price">Tổng tiền<span class="small text-danger">*</span></label>
                                    <input type="text" id="name" class="form-control" name="price" value="{{ old('price') }}" maxlength="500" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group focused">
                                    <label class="form-control-label" for="description">Mô tả
                                        <span class="small text-danger">*</span>
                                    </label>
                                    <textarea rows="5" id="description" class="form-control" name="description" maxlength="500" required>{{ old('description') }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col text-left">
                                <button type="submit" class="btn btn-success save submit">Tạo Mới</button>
                                <a href="{{ route('admin.orders.index') }}" class="btn btn-secondary">Thoát</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection