@extends('layouts.admin.app')

@section('title', 'Đơn Hàng')

@section('main-content')

@include('admin.orders.search', ['route' => route('admin.orders.index')])

<form method="post">
    @csrf
    @method('POST')
    <div class="action-list col-12 mb-2" style="display:flex; justify-content:space-between; align-items: center;">
        <div class="amount-user">
            @if ($orders->total() <= 0) @else {!! $orders->total() !!} khách hàng
                @endif
        </div>
        <div class="text-right">
            <a href="{{ route('admin.orders.create') }}" class="text-success">Tạo mới</a>
        </div>
    </div>
    @if (session('successMsg'))
    <div class="alert alert-success" role="alert">
        {{ session('successMsg') }}
    </div>
    @endif
    <div class="card">
        <div class="card-body p-0">
            <div class="table-responsive">
                <table class="table m-0">
                    <thead>
                        <tr>

                            <th width="2%">ID</th>
                            <th>Tên Khách Hàng</th>
                            <th>Giá</th>
                            <th>Phương thức thanh toán</th>
                            <th width="12%">Trạng thái</th>

                            <th width="12%">
                                <form action="">Chức năng</form>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($orders->total() <= 0) <tr>
                            <td colspan="8" class="text-center text-danger bg-danger">
                                Không có giữ liệu
                            </td>
                            </tr>
                            @else
                            @foreach ($orders as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                              
                                <td>{{ $item->customer->full_name }}</td>

                                <td>{{ currency_format($item->price)}}</td>
                                @if ($item->payment_methods == 0)
                                <td>Chuyển khoản</td>
                                @else
                                <td>Tiền mặt</td>
                                @endif
                                @if ($item->status == 0)
                                <td>Mới</td>
                                @elseif($item->status == 1)
                                <td>Thành công</td>
                                @else
                                <td>Thất bại</td>
                                @endif
                                <td>
                                    <form action="{{ route('admin.orders.destroy', $item->id) }}" method="POST" style="float: left; margin-right: 7px">
                                        <a class="btn btn-success btn-sm mr-1" href="{{ route('admin.order_detail.show', $item->id) }}"><i class="far fa-list-alt"></i></a>
                                        <a class="btn btn-success btn-sm mr-1" href="{{ route('admin.orders.edit', $item->id) }}"><i class="fas fa-pencil-alt"></i></a>
                                        <a href="javascript:void(0)" class="btn btn-danger btn-sm delete-d" id="{{ $item->id }}" data="orders"><i class="fas fa-trash"></i></a>
                                    </form>

                                    {{-- //Modal Delete --}}
                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">
                                                        Bạn có muốn xóa không ?
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <h5>Bạn có chắc chắn không ?</h5>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
                                                    <button type="button" class="btn btn-success" id="confirm-delete">Đồng ý</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </td>
                            </tr>


                            @endforeach
                            @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</form>
<div class="row">
    @if ($orders->total() <= 0) <p>
        </p>
        @else
        <div class="col-6">
            Hiển thị : {!! $orders->currentPage() * $orders->perPage() - $orders->perPage() + 1 !!} - {!! $orders->currentPage() * $orders->perPage() - $orders->perPage() + $orders->count()  !!}
            của {!! $orders->total() !!}
        </div>
        <div class="col-6">
            <div class="float-right">
                {!! $orders->appends(request()->except(['page', '_token']))->links() !!}
            </div>
        </div>
        @endif
</div>

@endsection


<?php
function currency_format($number, $suffix = '')
{
    if (!empty($number)) {
        return number_format($number, 0, ',', ',') . "{$suffix}";
    }
}
?>