@extends('layouts.admin.app')

@section('title', 'Đơn hàng')

@section('main-content')

@if ($errors->any())
<div class="alert alert-danger border-left-danger" role="alert">
    <ul class="pl-4 my-2">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@if (session('errorMsg'))
<div class="alert alert-danger" role="alert">
    {{ session('errorMsg') }}
</div>
@endif
@if (session('successMsg'))
<div class="alert alert-success" role="alert">
    {{ session('successMsg') }}
</div>
@endif

<div class="row">
    <div class="col-lg-12 order-lg-1">

        <div class="card shadow mb-4">

            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-success">Thông tin đơn hàng</h6>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('admin.orders.update', $orders->id) }}" autocomplete="off" id="myform">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="PUT">
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group focused">
                                    <label class="form-control-label" for="customer_id">Tên khách hàng<span class="small text-danger">*</span></label>
                                    <input type="text" id="name" class="form-control" name="customer_id" value="{{ old('customer_id', $orders->customer_id) }}" maxlength="500" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group focused">
                                    <label class="form-control-label" for="price">Tổng tiền<span class="small text-danger">*</span></label>
                                    <input type="text" id="name" class="form-control" name="price" value="{{ old('price', $orders->price) }}" maxlength="500" required>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="status">Tình trạng đơn hàng
                                        <span class="small text-danger">*</span></label>
                                    <input type="text" id="status" class="form-control" name="status" placeholder="example@example.com" value="{{ old('status', $orders->status) }}" maxlength="200" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group focused">
                                    <label class="form-control-label" for="payment_methods">Hình thức thanh toán
                                        <span class="small text-danger">*</span>
                                    </label>
                                    <input type="text" id="payment_methods" class="form-control" name="payment_methods" value="{{ old('payment_methods', $orders->payment_methods) }}" minlength="10" maxlength="10" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="description">Mô tả
                                        <span class="small text-danger">*</span></label>
                                    <input type="text" id="description" class="form-control" name="description" placeholder="example@example.com" value="{{ old('description', $orders->description) }}" maxlength="200" required>
                                </div>
                            </div>
                        
                        </div>

                       
                    </div>
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col text-left">
                                <button type="submit" class="btn btn-success save">Lưu</button>

                                <a href="{{ route('admin.orders.index') }}" class="btn btn-secondary">Thoát</a>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection