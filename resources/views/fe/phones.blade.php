<div class="menu_phone menu_product col-lg-12 mx-auto">
    <div class=" d-flex title pt-3 pb-3 text-left col-lg-10 mx-auto">
        <h3>Điện Thoại <i class="fas fa-mobile-alt"></i></h3>
    </div>
    <div class="col-lg-10 mx-auto list_product list_phone">
        @if (count($phones) <= 0) <h3> Không có giữ liệu </h3>
            @else
            @foreach ($phones as $item)
            <div class="col-lg-3 col-md-4 col-12 ">
                <div class="one_phone one_product col-lg-12">
                    <div class="one_phone-image one_product_image">
                        <img src="{{ asset('img/'. $item->picture) }}" alt="sanpham" class="pt-4 pb-4 w-50"/>
                        <div class="addtocart d-flex">
                            <p class="quick-view">xem sản phẩm</p>
                            <p class="add">thêm vào giỏ</p>
                        </div>
                    </div>
                    <div class="one_phone-name one_product_name">
                        <p>{{ $item->name }}</p>
                        <div class="price money_red">{{ $item->price }}</div>
                        <div class="stars">
                            <i class="fas fa-star star"></i>
                            <i class="fas fa-star star"></i>
                            <i class="fas fa-star star"></i>
                            <i class="fas fa-star star"></i>
                            <i class="fas fa-star star"></i>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
    </div>
</div>