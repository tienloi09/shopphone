<div class="flash_sale">
        <div class="d-flex title pt-3 pb-3 text-left col-lg-10 mx-auto">
            <h3>F<span><i class="fas fa-bolt"></i></span>ash sale online</h3>
        </div>
        <div class="list col-lg-10 mx-auto ">
            <div class="owl-carousel list_phone owl-theme">
                <div class="one_phone col-lg-12">
                    <img src="{{ asset('img/images/phone1.png') }}" alt="">
                    <div class="content">
                        <a href="#">Samsung Galaxy A12 - Chính Hãng</a>
                        <span class="money_red"> 3,590,000 đ</span>
                        <span class="money_black">4,290,000 đ</span>
                    </div>
                </div>
                <div class="one_phone  col-lg-12">
                    <img src="{{ asset('img/images/phone2.png') }}" alt="">
                    <div class="content">
                        <a href="#">Samsung Galaxy A12 - Chính Hãng</a>
                        <span class="money_red"> 3,590,000 đ</span>
                        <span class="money_black">4,290,000 đ</span>
                    </div>
                </div>


                <div class="one_phone  col-lg-12">
                    <img src="{{ asset('img/images/phone3.png') }}" alt="">
                    <div class="content">
                        <a href="#">Samsung Galaxy A12 - Chính Hãng</a>
                        <span class="money_red"> 3,590,000 đ</span>
                        <span class="money_black">4,290,000 đ</span>
                    </div>
                </div>


                <div class="one_phone  col-lg-12">
                    <img src="{{ asset('img/images/phone4.png') }}" alt="">
                    <div class="content">
                        <a href="#">Samsung Galaxy A12 - Chính Hãng</a>
                        <span class="money_red"> 3,590,000 đ</span>
                        <span class="money_black">4,290,000 đ</span>
                    </div>
                </div>


                <div class="one_phone col-lg-12">
                    <img src="{{ asset('img/images/phone5.png') }}" alt="">
                    <div class="content">
                        <a href="#">Samsung Galaxy A12 - Chính Hãng</a>
                        <span class="money_red"> 3,590,000 đ</span>
                        <span class="money_black">4,290,000 đ</span>
                    </div>
                </div>


                <div class="one_phone col-lg-12">
                    <img src="{{ asset('img/images/phone6.png') }}" alt="">
                    <div class="content">
                        <a href="#">Samsung Galaxy A12 - Chính Hãng</a>
                        <span class="money_red"> 3,590,000 đ</span>
                        <span class="money_black">4,290,000 đ</span>
                    </div>
                </div>


                <div class="one_phone col-lg-12">
                    <img src="{{ asset('img/images/phone7.png') }}" alt="">
                    <div class="content">
                        <a href="#">Samsung Galaxy A12 - Chính Hãng</a>
                        <span class="money_red"> 3,590,000 đ</span>
                        <span class="money_black">4,290,000 đ</span>
                    </div>
                </div>

                <div class="one_phone col-lg-12">
                    <img src="{{ asset('img/images/phone8.png') }}" alt="">
                    <div class="content">
                        <a href="#">Samsung Galaxy A12 - Chính Hãng</a>
                        <span class="money_red"> 3,590,000 đ</span>
                        <span class="money_black">4,290,000 đ</span>
                    </div>
                </div>
            </div>
        </div>
    </div>