<div class="menu_laptop menu_product col-lg-12 mx-auto">
        <div class=" d-flex title pt-3 pb-3 text-left col-lg-10 mx-auto">
            <h3>Laptop <i class="fas fa-laptop"></i></h3>
        </div>
        <div class="col-lg-10 mx-auto list_product list_laptop">
        @if (count($laptops) <= 0) <h3> Không có giữ liệu </h3>
            @else
            @foreach ($laptops as $item)
            <div class="col-lg-3 col-md-4 col-12">
                <div class="one_laptop one_product col-lg-12">
                    <div class="one_laptop-image one_product_image">
                        <img src="{{ asset('img/'. $item->picture) }}"  alt="sanpham" class="pt-4 pb-4 w-50"/>
                        <div class="addtocart d-flex">
                            <p class="quick-view">xem sản phẩm</p>
                            <p class="add">thêm vào giỏ</p>
                        </div>
                    </div>
                    <div class="one_laptop-name one_product_name">
                        <p>{{ substr($item->name, 0, strpos(wordwrap($item->name, 30), "\n"))  }} ...</p>
                        <div class="price money_red money_red">{{ $item->price }}</div>
                        <div class="stars">
                            <i class="fas fa-star star"></i>
                            <i class="fas fa-star star"></i>
                            <i class="fas fa-star star"></i>
                            <i class="fas fa-star star"></i>
                            <i class="fas fa-star star"></i>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>