<header>
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="row head col-lg-10 col-12 mx-auto">
                <div
                    class="head__top d-flex flex-wrap justify-content-between col-lg-12 col-sm-12 col-md-12 col-12 pt-2 pb-2">
                    <a href="#" class="hamburger position-absolute">
                        <i class="fal fa-align-left"></i>
                    </a>
                    <a href="/trang-chu" class="logo col-sm-12 col-md-12 col-12 col-lg-3 col-md-3">Khải anh</a>
                    <form action="" class="position-relative form-search col-sm-12 col-12 col-lg-6 col-md-12">
                        <input type="text" placeholder="hôm nay bạn cần tìm gì?">
                        <i class="fas fa-search position-absolute"></i>
                    </form>
                    <a href="#" class="bag">
                        <i class="fas fa-shopping-basket"></i>
                    </a>
                </div>
            </div>
            <div class="row col-lg-10 col-sm-12 col-md-12 mx-auto">
                <div class="menu head__bottom">
                    <ul class="nav-list d-flex justify-content-between">
                        <li class="nav-item"><a href="/dien-thoai" class="nav-link"><i class="fas fa-mobile-alt"></i> <span>Điện
                                    Thoại</span></a></li>
                        <li class="nav-item"><a href="/dong-ho" class="nav-link"><i class="far fa-clock"></i> <span>Đồng Hồ
                                </span> </a></li>
                        <li class="nav-item"><a href="/laptop" class="nav-link"><i class="fas fa-laptop"></i> <span>Laptop -
                                    Máy tính</span> </a></li>
                        <li class="nav-item"><a href="#" class="nav-link"><i class="fas fa-tablet-alt"></i>
                                <span>Tablet</span> </a></li>
                        <li class="nav-item"><a href="#" class="nav-link"><i class="fas fa-headphones-alt"></i>
                                <span>Loa - Tai nghe</span> </a></li>
                        <li class="nav-item"><a href="/phu-kien" class="nav-link"><i class="far fa-keyboard"></i> <span>Phụ kiện
                                </span> </a></li>
                        <li class="nav-item"><a href="/sua-chua" class="nav-link"><i class="fas fa-tools"></i><span>Sửa chữa
                                </span></a></li>
                        <li class="nav-item"><a href="/sim-the" class="nav-link"><i class="fas fa-sim-card"></i><span>Sim thẻ
                                </span></a></li>
                        <li class="nav-item"><a href="/tin-tuc" class="nav-link"><i class="far fa-newspaper"></i><span>Tin tức
                                </span></a></li>
                        <li class="nav-item"><a href="/khuyen-mai" class="nav-link"><i class="fas fa-fire-alt"></i><span>Khuyến
                                    mại </span></a></li>
                    </ul>
                </div>
            </div>
        </div>

        <!--------------------------------menu-secondary ----------------------------------------------->
        <div class="menu-secondary">
            <ul class="">
                <li class="login position-relative"><a href="./login/login.html" class=""><i
                            class="far fa-user"></i><span>Đăng nhập</span></a><span
                        class="position-absolute hamburger"><i class="fal fa-align-left"></i></span></li>
                <li class=""><a href="#" class=""><i class="fas fa-mobile-alt"></i><span>Điện Thoại</span></a></li>
                <li class=""><a href="#" class=""><i class="far fa-clock"></i><span>Đồng Hồ </span> </a></li>
                <li class=""><a href="#" class=""><i class="fas fa-laptop"></i><span>Laptop - Máy tính</span> </a></li>
                <li class=""><a href="#" class=""><i class="fas fa-tablet-alt"></i><span>Tablet</span> </a></li>
                <li class=""><a href="#" class=""><i class="fas fa-headphones-alt"></i><span>Loa - Tai nghe</span> </a>
                </li>
                <li class=""><a href="#" class=""><i class="far fa-keyboard"></i><span>Phụ kiện </span> </a></li>
                <li class=""><a href="#" class=""><i class="fas fa-tools"></i><span>Sửa chữa </span></a></li>
                <li class=""><a href="#" class=""><i class="fas fa-sim-card"></i><span>Sim thẻ </span></a></li>
                <li class=""><a href="#" class=""><i class="far fa-newspaper"></i><span>Tin tức </span></a></li>
                <li class=""><a href="#" class=""><i class="fas fa-fire-alt"></i><span>Khuyến mại </span></a></li>
            </ul>
        </div>
        <div id="screen_black" class="d-none"></div>
    </header>