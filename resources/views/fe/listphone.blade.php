<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách điện thoại</title>
    <!-- google font -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Andika+New+Basic:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <!-- fontawesome cdn -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css">

    <!-- bootstrap 5 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"></script>

    <!-- <script src="./assets/jquery-3.6.0.min.js"></script> -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>

    <script rel="stylesheet" src="{{ asset('js/frontend.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" />

    <!-- countdown_jq   -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

    <!-- <link rel="stylesheet" href="./css/style.css"> -->
    <link rel="stylesheet" href="{{ asset('css/fe.css') }}">
    <link rel="stylesheet" href="./assets/reponsive.css">
</head>

<body>
    @include('fe.header')

    <span id="scroll_to_top"><i class="fas fa-caret-square-up"></i></span>

    <div class="menu_phone menu_product col-lg-12 mx-auto">
        <div class=" d-flex title pt-3 pb-3 text-left col-lg-10 mx-auto">
            <h3>Điện Thoại <i class="fas fa-mobile-alt"></i></h3>
        </div>
        <div class="col-lg-10 mx-auto list_product list_phone">
            @if (count($phones) <= 0) <h3> Không có giữ liệu </h3>
                @else
                @foreach ($phones as $item)
                <div class="col-lg-3 col-md-4 col-12 ">
                    <div class="one_phone one_product col-lg-12">
                        <div class="one_phone-image one_product_image">
                            <img src="{{ asset('img/'. $item->picture) }}" alt="sanpham" class="pt-4 pb-4 w-50" />
                            <div class="addtocart d-flex">
                                <p class="quick-view">xem sản phẩm</p>
                                <p class="add">thêm vào giỏ</p>
                            </div>
                        </div>
                        <div class="one_phone-name one_product_name">
                            <p>{{ $item->name }}</p>
                            <div class="price money_red">{{ $item->price }}</div>
                            <div class="stars">
                                <i class="fas fa-star star"></i>
                                <i class="fas fa-star star"></i>
                                <i class="fas fa-star star"></i>
                                <i class="fas fa-star star"></i>
                                <i class="fas fa-star star"></i>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
        </div>
    </div>
    @include('fe.footer')



    <script src="./assets/main.js"></script>
</body>

</html>