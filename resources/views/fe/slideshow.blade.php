<div class="slideshow">
        <div class="col-lg-10 col-12 mx-auto d-flex flex-wrap">
            <div class="col-lg-12 col-12 slideshow__images">
                <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0"
                            class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
                            aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"
                            aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="{{ asset('img/images/slideshow1.png') }}" class="d-block w-100 " alt="...">
                            <div class="slide_text slide_text_right">
                                <h2 class="animate__animated animate__fadeInRight  animate__delay-0.5s">IPHONE 13 PRO
                                </h2>
                                <p class="animate__animated animate__fadeInRight  animate__delay-0.5s"> FULL SCREEN
                                    DISPLAY</p>
                                <a href="#"
                                    class="button-1 animate__animated animate__fadeInRight animate__delay-1s">Mua
                                    ngay</a>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="{{ asset('img/images/slideshow2.png') }}" class="d-block w-100 " alt="...">
                            <div class="slide_text slide_text_right">
                                <h2 class="animate__animated animate__fadeInRight animate__delay-.5s">IPHONE 14 PRO</h2>
                                <p class="animate__animated animate__fadeInRight animate__delay-.5s"> CURVY BEVEL DUAL
                                    AUDIO</p>
                                <a href="#"
                                    class="button-1 animate__animated animate__fadeInRight animate__delay-1s">Mua
                                    ngay</a>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="{{ asset('img/images/slideshow3.png') }}" class="d-block w-100 " alt="...">
                            <div class="slide_text slide_text_left">
                                <h2 class=" animate__animated animate__fadeInLeft animate__delay-.5s">IPHONE 15 PRO</h2>
                                <p class="animate__animated animate__fadeInLeft animate__delay-.5s"> CURVY BEVEL DUAL
                                    AUDIO</p>
                                <a href="#" class="button-1 animate__animated animate__fadeInLeft animate__delay-1s">Mua
                                    ngay</a>
                            </div>
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
                        data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
                        data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
        </div>

    </div>