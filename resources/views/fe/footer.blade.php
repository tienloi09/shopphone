<footer>
        <div class="col-lg-10 footer">
            <div class="col-lg-3">
                <h4>Hỗ trợ - Dịch vụ</h4>
                <a href="#">Mua hàng trả góp</a>
                <a href="#">Hướng dẫn đặt hàng và thanh toán</a>
                <a href="#">Chính sách bảo hành</a>
                <a href="#">Tra cứu đơn hàng</a>
            </div>
            <div class="col-lg-3">
                <h4>Thông tin liên hệ</h4>
                <a href="#">Bán hàng online</a>
                <a href="#">Chăm sóc khách hàng</a>
                <a href="#">Hỗ trợ kĩ thuật</a>
                <a href="#">Hỗ trợ bảo hành sửa chữa</a>
            </div>
            <div class="col-lg-3">
                <h4>Hệ thống 99 siêu thị trên toàn quốc </h4>
                <a href="#">Danh sách 99 siêu thị</a>
            </div>
            <div class="col-lg-3 ">
                <h4>Liên hệ cửa hàng</h4>
                <div class="icon">
                    <span><i class="fab fa-youtube"></i></span>
                    <span><i class="fab fa-facebook"></i></span>
                    <span><i class="fab fa-facebook-messenger"></i></span>
                </div>

            </div>
        </div>
    </footer>