<?php

namespace App\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class Order_detailRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'price' => 'required',
            'quantity'       => 'required',
            'order_id'       => 'required',
            'phone_id'       => '',
            'laptop_id'       => '',
            'headphones_id'       => '',
            'tablet_id'       => '',
        ];
    }
    public function messages()
    {
        return [
            'price.required'        => 'Please fill the price.',
            'quantity.required'        => 'Please fill the quantity.',
            'order_id.required'        => 'Please fill the order_id.',
      
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json(
            [
                'error'       => $errors,
                'status_code' => 422,
            ],
            JsonResponse::HTTP_UNPROCESSABLE_ENTITY
        ));
    }
}
