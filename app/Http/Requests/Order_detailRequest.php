<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class Order_detailRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'order_id' => 'required',
            'quantity'     => 'required',
            'price'      => 'required',
         
       
        ];
    }
    public function messages()
    {
        return [
            'order_id.required'        => 'Please fill the order_id.',
            'quantity.required'        => 'Please fill the quantity.',
            'price.required'        => 'Please fill the price.',
        ];
    }

}
