<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class Order_detailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id'                   => $this->id,
            'tablet_id'              => $this->tablet_id,
            'phone_id'           => $this->phone_id,
            'laptop_id'              => $this->laptop_id,
            'headphones_id'              => $this->headphone_id,
            'order_id'              => $this->order_id,
            'price'              => $this->price,
            'quantity'              => $this->quantity,
            'status'              => $this->status,
            'description'              => $this->description,
            'join_date'              => $this->join_date,

            'created_at'  => (string) $this->created_at,
            'updated_at'  => (string) $this->updated_at,
        ];
    }
}
