<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class OrdersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id'                   => $this->id,
            'customer_id'              => $this->customer_id,
            'price'           => $this->price,
            'description'              => $this->description,
            'status'              => $this->status,
            'payment_methods'              => $this->payment_methods,
            'join_date'              => $this->join_date,

            'created_at'  => (string) $this->created_at,
            'updated_at'  => (string) $this->updated_at,
        ];
    }
}
