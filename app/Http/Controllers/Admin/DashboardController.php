<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Customers;
use App\Models\Orders;

class DashboardController extends Controller
{
    public function index()
    {
        $customers = Customers::all();
        $orders = Orders::all();
        $order_win = $orders->where('status', 1);
        $total = 0;
        foreach ($order_win as $money) {
            $total += $money->price;
        }
        $data = [
            'total_customer' => count($customers),
            'order_new'      => count($orders->where('status', 0)),
            'order_win'    => count($order_win),
            'total_money'         => $total,
        ];

        return view('admin.dashboard.index', compact('data'));
    }
}
