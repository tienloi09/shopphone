<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order_detail;
use Carbon\Carbon;
use App\Http\Requests\Order_detailRequest;
use App\Http\Controllers\Controller;
use App\Models\Phones;
use App\Models\Laptops;
use Illuminate\Support\Facades\DB;

class Order_detailController extends Controller

{

    public function index()
    {
    
        $paging_number = 10;
        $data = request()->post();
        $orders = Order_detail::search($data);
      
        return view('admin.order_detail.list', compact('orders'))
            ->with('i', (request()->input('page', 1) - 1) * $paging_number);
    }
    public function show($id)
    {    
       
        $phones = Phones::all();
        $laptops = Laptops::all();
        
        $order_detail = DB::table('order_detail')->where('order_id', $id)->get();
    
        return view('admin.order_detail.list', compact('order_detail','laptops','phones'));
    }
    public function create()
    {
        return view('admin.orders.create');
    }

    public function store(Order_detailRequest $request)
    {   
       
        $data = $request->all();
        Order_detail::create($data);
        return redirect()->route('admin.orders.index')
            ->with('success', 'orders created successfully.');
    }

    

    public function edit($id)
    {
        $orders = Order_detail::find($id);
        return view('admin.orders.edit', compact('orders'));
    }
    public function update(Order_detailRequest $request, $id)
    {
            $orders = Order_detail::find($id);
            $orders->update($request->all());
            return redirect()->route('admin.orders.index')
                ->with('success', 'orders updated successfully');
       
    }
    public function destroy($id)
    { 

        $result = Order_detail::find($id);
        $result->delete();
        $result->roles()->detach();
        return json_encode(['success' => 'deleted successfully']);
    }

}
