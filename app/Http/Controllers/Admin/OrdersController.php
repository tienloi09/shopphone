<?php

namespace App\Http\Controllers\Admin;

use App\Models\Orders;
use Carbon\Carbon;
use App\Http\Requests\OrdersRequest;
use App\Http\Controllers\Controller;
use App\Models\Customers;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller

{

    public function index()
    {
    
        $paging_number = 10;
        $data = request()->post();
        $orders = Orders::search($data);
        return view('admin.orders.list', compact('orders'))
            ->with('i', (request()->input('page', 1) - 1) * $paging_number);
    }
    public function show($id)
    {
        $orders = Orders::find($id);
        return view('admin.orders.show', compact('orders'));
    }
    public function create()
    {
        return view('admin.orders.create');
    }

    public function store(OrdersRequest $request)
    {   
       
        $data = $request->all();
        Orders::create($data);
        return redirect()->route('admin.orders.index')
            ->with('success', 'orders created successfully.');
    }

    

    public function edit($id)
    {
        $orders = Orders::find($id);
        return view('admin.orders.edit', compact('orders'));
    }
    public function update(OrdersRequest $request, $id)
    {
            $orders = Orders::find($id);
            $orders->update($request->all());
            return redirect()->route('admin.orders.index')
                ->with('success', 'orders updated successfully');
       
    }
    public function destroy($id)
    { 

        $result = Orders::find($id);
        $result->delete();
        $result->roles()->detach();
        return json_encode(['success' => 'deleted successfully']);
    }

}
