<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Phones;

class PhoneController extends Controller
{
    public function index(){
           
          $phones = Phones::all();
          
          return view('fe.listphone', compact('phones'));
    }
 
}
