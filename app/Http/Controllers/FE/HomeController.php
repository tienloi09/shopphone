<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Phones;
use App\Models\Laptops;
class HomeController extends Controller
{
    public function index(){
        
          $phones=  Phones::orderBy('id','desc')->limit(8)->get();
          $laptops=  Laptops::orderBy('id','desc')->limit(8)->get();
          
          return view('fe.home', compact('phones','laptops'));
    }

}
