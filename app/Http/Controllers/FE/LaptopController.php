<?php

namespace App\Http\Controllers\FE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Laptops;

class LaptopController extends Controller
{
    public function index(){
           
          $laptops = Laptops::all();
          
          return view('fe.listlaptop', compact('laptops'));
    }
 
}
