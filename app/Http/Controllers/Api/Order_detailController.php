<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Order_detailRequest;
use App\Http\Resources\Api\Order_detailResource;
use App\Models\Order_detail;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
class Order_detailController extends Controller
{
    public function index(){

        $paging_number = Order_detail::paginate(10);
        return Order_detailResource::collection($paging_number);
    }
    public function show($id)
    {
        $result = Order_detail::find($id);
        return new Order_detailResource($result);
    }
    public function store(Order_detailRequest $request){
        
        try {
            $result = new Order_detail();
     
            $result->fill($request->validated())->save();
             return new Order_detailResource($result);

        } catch (\Exception $exception) {
            return response()->json(['message' => 'Could not create a new order_detail', 'error' => "Invalid data - {$exception->getMessage()}"], 400);
        }
    }

    public function update(Order_detailRequest $request, $id)
    {
        $result = Order_detail::find($id);

        if (!empty($result)) {
            $result->fill($request->validated())->save();

            return new Order_detailResource($result);
        } else {
            return response()->json(['message' => 'Acc not found'], 404);
        }
    }

    public function destroy($id)
    {
        $result = Order_detail::find($id);

        if (!empty($result)) {
            $result->delete();
        }

        return response()->json(null, 204);
    }
}
