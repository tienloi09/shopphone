<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Orders extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'orders';
    protected $fillable = ['customer_id','price','description','status','payment_methods'];

    public function customer()
    {
        return $this->belongsTo(Customers::class,'customer_id');
    }
    public function scopeSearch($query, $data)
    {
        $paging_number = 10;

        if (!empty($data)) {
            foreach ($data as $key => $value) {
                if(!empty($value)) {
                    switch ($key) {
                        case 'q':
                            $query->where(function ($q) use ($value) {
                                $q->where('price', 'LIKE', "%$value%");
                            });
                            // ->orWhere('customers.full_name', 'LIKE', '%' . $value . '%')->join('customers', $this->table . '.customer_id', '=', 'customers.id')
                            break;
                     
                     
                        case 'status':
                            $query->where('status', $value);
                            break;
                    }
                }
            }
        }

        return $query->orderBy('created_at', 'desc')->paginate($paging_number);
    }
}
