<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Order_detail extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'order_detail';
    protected $fillable = ['tablet_id','phone_id','laptop_id','headphone_id','order_id','price','quantity','description','status'];
    public function scopeSearch($query, $data)
    {
        $paging_number = 10;

        if (!empty($data)) {
            foreach ($data as $key => $value) {
                if(!empty($value)) {
                    switch ($key) {
                        case 'q':
                            $query->where(function ($q) use ($value) {
                                $q->where('phone', 'LIKE', "%$value%")->orWhere('customers.full_name', 'LIKE', '%' . $value . '%')->join('customers', $this->table . '.customer_id', '=', 'customers.id');
                            });
                            break;
                        case 'gender':
                            $query->where('gender', $value);
                            break;
                    }
                }
            }
        }

        return $query->orderBy('created_at', 'desc')->paginate($paging_number);
    }
}
