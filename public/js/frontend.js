document.addEventListener("DOMContentLoaded", function () {
    var hamburger = document.getElementsByClassName('hamburger');
    var menu_secondary = document.getElementsByClassName('menu-secondary');
    var screen_black = document.getElementById('screen_black');


    //---------menu_out------------------------------------
    hamburger[0].onclick = function () {
        menu_secondary[0].classList.add('menu_out');
        menu_secondary[0].classList.add('menu_out');
        screen_black.classList.remove('d-none');
    }
    hamburger[1].onclick = function () {
        menu_secondary[0].classList.remove('menu_out');
        screen_black.classList.add('d-none');
    }
    screen_black.onclick = function () {
        screen_black.classList.add('d-none');
        menu_secondary[0].classList.remove('menu_out');
    }



    //---------flash_sale------------------------------------

    $('.owl-carousel').owlCarousel({
        loop: true,
        items: 4,
        autoplay: true,
        dots: false,
        autoplayTimeout: 5000,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            },

            1200: {
                items: 5
            }
        }
    })



    //----------scroll_to_top-----------------------------------------
    var height_body = $('body').height();
    var height_nav = $('.head').height();
    console.log(height_nav);
    var result = 0;
    $(window).scroll(function () {
        var pos_body = $('html,body').scrollTop();
        if (pos_body > height_body / 3) {
            if (result == 0) {
                $('#scroll_to_top').addClass('out');
                result = 1;
            }
        } else {
            if (result == 1) {
                $('#scroll_to_top').removeClass('out');
                result = 0;
            }
        }
        if(pos_body >= height_nav){
            $('.head__bottom').addClass('fixed');
            $('.head').addClass('d-none');
             
        }else{
            $('.head__bottom').removeClass('fixed');
            $('.head').removeClass('d-none');
        }
    })
    $('#scroll_to_top').click(function () {
        window.scrollTo({ top: 0, behavior: "smooth" });
    })


});


